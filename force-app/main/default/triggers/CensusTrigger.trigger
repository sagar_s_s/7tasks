trigger CensusTrigger on Census_Member__c (after insert,after update,before insert,before update) {
    
    if(Trigger.isAfter && Trigger.isInsert) {
        CensusMemberHandler.onAfterInsert(Trigger.New);
    }
    
    if(Trigger.isAfter && Trigger.isUpdate) {
        CensusMemberHandler.onAfterUpdate(Trigger.newMap,Trigger.oldMap,Trigger.New);
    }
    if(Trigger.isBefore && Trigger.isInsert) {
        CensusMemberHandler.onBeforeInsert(Trigger.New);
    }
    if(Trigger.isBefore && Trigger.isUpdate) {
        CensusMemberHandler.onBeforeUpdate(Trigger.newMap,Trigger.oldMap);
    }
}