@isTest public class CensusTestClass {
    
    @isTest public static void onAfterInsert(){
        Account objAcc1 = new Account( Name ='TestAccount20');
        insert objAcc1;
        Census__c objCen1 = new Census__c ( Name ='Censussag' ,vlocity_ins_GroupId__c = objAcc1.Id);
        insert objCen1;
        
        Census__c objCenNew1 = [SELECT Id, Name, vlocity_ins_GroupId__c FROM Census__c];
        system.debug('objCenNew1' +objCenNew1);
        //Census_Member__c objCm = new Census_Member__c();
        Census_Member__c objCm1 = new Census_Member__c();
        objCm1.vlocity_ins_IsPrimaryMember__c = True;
        objCm1.Name = 'Silverlineerm';
        //objCm1.vlocity_ins_IsPrimaryMember__c = TRUE; 
        objCm1.vlocity_ins_CensusId__c = objCenNew1.Id;
        Test.startTest();
         insert objCm1;
        //objCm1.vlocity_ins_IsPrimaryMember__c = FALSE; 
        //update objCm1;
        Test.stopTest();
        List<Census__c> lstCns = [SELECT Name, Primary_Member__c FROM Census__c];
        System.assertEquals(1, lstCns.size());
    }
    @isTest public static void onAfterUpdate() {
        Account objAcc2 = new Account( Name ='TestAccount20');
        insert objAcc2;
        Census__c objCen2 = new Census__c ( Name ='Censussaggg' ,vlocity_ins_GroupId__c = objAcc2.Id);
        insert objCen2;
        Census__c objjCenNew = [SELECT Id, Name, vlocity_ins_GroupId__c FROM Census__c];
        system.debug('objjCenNew' +objjCenNew);
        Census_Member__c objCenmem = new Census_Member__c(); 
        objCenmem.Name = 'Silverlinecrm';
        objCenmem.vlocity_ins_IsPrimaryMember__c = FALSE; 
        objCenmem.vlocity_ins_CensusId__c = objjCenNew.Id;
        insert objCenmem;
        objCenmem.vlocity_ins_IsPrimaryMember__c = FALSE; 
        Test.startTest();
        update objCenmem;
        Test.stopTest();
        List<Census__c> lstCnns = [SELECT Name, Primary_Member__c FROM Census__c];
        System.assertEquals(1, lstCnns.size());
        
    }
    @isTest public static void onBeforeInsertTwo(){
        
        Account objAcc = new Account( Name ='TestAccount');
        insert objAcc;
        Census__c objCen = new Census__c ( Name ='Census01' ,vlocity_ins_GroupId__c = objAcc.Id,vlocity_ins_EffectiveStartDate__c = Date.newInstance(2022, 12, 29),vlocity_ins_EffectiveEndDate__c = Date.newInstance(2021, 12, 31));
        insert objCen;
        
        Census__c objCenNew = [SELECT Id, Name, vlocity_ins_GroupId__c,vlocity_ins_EffectiveStartDate__c,vlocity_ins_EffectiveEndDate__c FROM Census__c];
        system.debug('objCenNew' +objCenNew);
        Census_Member__c objCm = new Census_Member__c(); 
        objCm.Name = 'Silverline'; 
        objCm.vlocity_ins_CensusId__c = objCenNew.Id;
        //objCm.htcs_EffectiveDate__c = Date.newInstance(2021, 12, 29);
        //objCm.htcs_TermDate__c = Date.newInstance(2021, 12, 31);
        Test.startTest();
        try{
            objCm.htcs_EffectiveDate__c = Date.newInstance(2022, 12, 29);
            objCm.htcs_TermDate__c = Date.newInstance(2022, 12, 31);
            insert objCm;
        }
        catch(Exception e){
            String message = e.getMessage();
            System.assert(message.contains('Invalid Dates,Plz enter correct dates'),'message=' +message);  
        } 
        Test.stopTest();
    }
    @isTest public static void onBeforeUpdateTwo() {
        Account objAcc = new Account( Name ='TestAccount');
        insert objAcc;
        Census__c objCen = new Census__c (Name ='Census01' ,vlocity_ins_GroupId__c = objAcc.Id,vlocity_ins_EffectiveStartDate__c = Date.newInstance(2021, 12, 29),vlocity_ins_EffectiveEndDate__c = Date.newInstance(2021, 12, 31));
        insert objCen;
        Census__c objjCenNew = [SELECT Id, Name, vlocity_ins_GroupId__c,vlocity_ins_EffectiveStartDate__c,vlocity_ins_EffectiveEndDate__c FROM Census__c];
        system.debug('objjCenNew' +objjCenNew);
        Census_Member__c objCenmem = new Census_Member__c(); 
        objCenmem.Name = 'Silverlinecrm';
        objCenmem.htcs_EffectiveDate__c = Date.newInstance(2021, 12, 29);
        objCenmem.htcs_TermDate__c = Date.newInstance(2021, 12, 31);
        objCenmem.vlocity_ins_CensusId__c = objjCenNew.Id;
        insert objCenmem;
        //objCenmem.htcs_EffectiveDate__c = Date.newInstance(2022, 12, 29);
        //objCenmem.htcs_TermDate__c = Date.newInstance(2022, 12, 31);
        Test.startTest();
        try {
            objCenmem.htcs_EffectiveDate__c = Date.newInstance(2022, 12, 29);
            objCenmem.htcs_TermDate__c = Date.newInstance(2022, 12, 31);
            update objCenmem;
        }
        catch(Exception e){
            String message = e.getMessage();
            System.assert(message.contains('Check the dates,Enter it in correct way'),'message=' +message);  
        } 
        Test.stopTest();
    }
    @isTest public static void onBeforeInsertThree(){
        
        Account objAcc = new Account( Name ='TestAccount');
       insert objAcc;
        Census__c objCen = new Census__c ( Name ='Census01' ,vlocity_ins_GroupId__c = objAcc.Id);
       insert objCen;
        
        Census__c objCenNew = [SELECT Id,vlocity_ins_GroupId__c FROM Census__c ];
        system.debug('objCenNew' +objCenNew);
        Census_Member__c objCm = new Census_Member__c(); 
         Test.startTest();
        try{
            objCm.Name = 'Silverline';
            objCm.vlocity_ins_IsPrimaryMember__c = FALSE; 
            objCm.vlocity_ins_CensusId__c = objCenNew.Id;
            insert objCm;
        }
        catch(Exception e){
            String message = e.getMessage();
            System.assert(message.contains('Primary memebr is present in the census'),'message=' +message);  
        } 
        Test.stopTest();
    }
    @isTest public static void onBeforeUpdateThree(){
        Account objAcc = new Account( Name ='TestAccount');
        insert objAcc;
        Census__c objCen = new Census__c ( Name ='Census01' ,vlocity_ins_GroupId__c = objAcc.Id);
        insert objCen;
        Census__c objjCenNew = [SELECT Id,vlocity_ins_GroupId__c FROM Census__c];
        system.debug('objjCenNew' +objjCenNew);
        Census_Member__c objCenmem = new Census_Member__c();
        Census_Member__c objCenmem1 = new Census_Member__c();
        objCenmem.Name = 'Silverlinecrm';
        objCenmem.vlocity_ins_IsPrimaryMember__c = FALSE; 
        objCenmem.vlocity_ins_CensusId__c = objjCenNew.Id;
        insert objCenmem;
        objCenmem.vlocity_ins_IsPrimaryMember__c = FALSE; 
        objCenmem1.Name = 'Silverlinecrm1';
        objCenmem1.vlocity_ins_IsPrimaryMember__c = FALSE; 
        objCenmem1.vlocity_ins_CensusId__c = objjCenNew.Id;
        insert objCenmem1;
        objCenmem1.vlocity_ins_IsPrimaryMember__c = FALSE;
        Test.startTest();
        try{
            objCenmem1.vlocity_ins_IsPrimaryMember__c = TRUE;
            update objCenmem1;
        }
        catch(Exception e){
            String message = e.getMessage();
            System.assert(message.contains('Primary memebr is present in the census'),'message=' +message);  
        } 
        Test.stopTest();
    }
    
    @isTest public static void afterInsertFour(){
        Account acc = new Account();
        acc.Name = 'Test';
        insert acc;
        Account objAc = [SELECT Id, Name FROM Account LIMIT 1];
        Census__c objCensus = new Census__c(Name = 'Maria', vlocity_ins_GroupId__c = objAc.Id);
        insert objCensus;
        Census__c objCen = [SELECT Id, Name FROM Census__c];
        Census_Member__c objCM = new Census_Member__c(Name = 'Sagar', vlocity_ins_CensusId__c = objCen.Id);
        insert objCM;
        Census_Member__c objCMs = new Census_Member__c(Name = 'Sagar', vlocity_ins_CensusId__c = objCen.Id);
        Test.startTest();
        insert objCMs;
        Test.stopTest();
    }
   
    @isTest public static void afterUpdateFour(){
        Account acc = new Account();
        acc.Name = 'Test';
        insert acc;
        Account objAc = [SELECT Id, Name FROM Account LIMIT 1];
        Census__c objCensus = new Census__c(Name = 'Maria',  vlocity_ins_GroupId__c= objAc.Id);
        insert objCensus;
        Census__c objCen = [SELECT Id, Name FROM Census__c];
        Census_Member__c objCMem = new Census_Member__c(Name = 'sheryaaa', vlocity_ins_CensusId__c = objCen.Id);
        insert objCMem;
        Census_Member__c objCMember = new Census_Member__c(Name = 'Sagar sheryaa', vlocity_ins_CensusId__c = objCen.Id);
        insert objCMember;
        Census_Member__c objNewCM = [SELECT Id, Name, vlocity_ins_CensusId__c, vlocity_ins_IsPrimaryMember__c FROM Census_Member__c WHERE Name = 'Sagar sheryaa' ];
       
        Test.startTest();
        objNewCM.Name = 'TestAccount';
        update objNewCM;
        Test.stopTest();
    }
}