public class JewelryHandler 
{
    public static void onBeforeInsert(List<Jewelry__c> lstjwlry) 
    {
        List<Jewelry__c> lstjwl = new List<Jewelry__c>();
        for(Jewelry__c objJwl : lstjwlry)
        {
            objJwl.Total_Price__c = objJwl.Unit_Price__c * objJwl.Quantity__c;
            lstjwl.add(objJwl);
        } 

    }
    public static void onBeforeUpdate(List<Jewelry__c> lstjwlry,Map<Id,Jewelry__c> oldJewelries) 
    {
        for(Jewelry__c objJewelry : lstjwlry) 
        {
            if(objJewelry.Quantity__c != oldJewelries.get(objJewelry.Id).Quantity__c || objJewelry.Unit_Price__c != oldJewelries.get(objJewelry.Id).Unit_Price__c)
            {
                objJewelry.Total_Price__c = objJewelry.Unit_Price__c * objJewelry.Quantity__c;
            }
        }
    }
}