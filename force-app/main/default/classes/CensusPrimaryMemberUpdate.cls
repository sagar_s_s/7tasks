public class CensusPrimaryMemberUpdate {
    public static void onBeforeInsertCensusPrimaryMemberUpdate(List<Census_Member__c> lstCensusMem) {
        set<Id> setPrimaryIds = new set<Id>();
        List<Census__c> lstCensus = [SELECT Id,Primary_Member__c FROM Census__c];
        for(Census__c objCensus : lstCensus) {
               setPrimaryIds.add(objCensus.Id);
        }
        for(Census_Member__c objCensusMem : lstCensusMem){
            if(setPrimaryIds.contains(objCensusMem.vlocity_ins_CensusId__c) && objCensusMem.vlocity_ins_IsPrimaryMember__c == TRUE){
                objCensusMem.addError('Primary memebr is present in the census');
            }
        }
    }
    public static void onBeforeUpdateCensusPrimaryMemberUpdate(Map<Id,Census_Member__c> mapNewMember, Map<Id,Census_Member__c> mapOldMember) {
        set<Id> setPrimaryIds = new set<Id>();
        List<Census__c> lstCensus = [SELECT Id,Primary_Member__c FROM Census__c];
        for(Census__c objCensus : lstCensus) {
            setPrimaryIds.add(objCensus.Id);
        }
        for(Id censusId : mapNewMember.keyset()) {
            
            Census_Member__c objCensusNew = mapNewMember.get(censusId);
            Census_Member__c objCensusOld = mapOldMember.get(censusId);
            if(setPrimaryIds.contains(mapNewMember.get(censusId).vlocity_ins_CensusId__c) && objCensusNew.vlocity_ins_IsPrimaryMember__c != objCensusOld.vlocity_ins_IsPrimaryMember__c)
            {
                 mapNewMember.get(censusId).addError('Primary memebr is present in the census');
            }
        }
    }
}