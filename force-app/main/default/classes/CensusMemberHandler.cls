public without sharing  class CensusMemberHandler 
{
    public static void onAfterInsert(List<Census_Member__c> lstCensusMem)
    {
        CensusMemberHandler.onAfterInsertCensusUpdate(lstCensusMem);
		CensusMemberHandler.onAfterInsertCensusRollUpMember(lstCensusMem);
    }
     
	
	public static void onBeforeInsert(List<Census_Member__c> lstCensusMem)
    {
        CensusMemberHandler.onBeforeInsertCensusMemberDateUpdate(lstCensusMem);
        CensusMemberHandler.onBeforeInsertCensusPrimaryMemberUpdate(lstCensusMem);
    }
		 	 
		 
	public static void onAfterUpdate(Map<Id,Census_Member__c> mapNewMember, Map<Id,Census_Member__c> mapOldMember,List<Census_Member__c> lstCensusMem)
    {
        CensusMemberHandler.onAfterUpdateCensusUpdate(mapNewMember, mapOldMember);
        CensusMemberHandler.onAfterUpdateCensusRollUpMember(lstCensusMem, mapOldMember);
            
    }
   
		
		
	public static void onBeforeUpdate(Map<Id,Census_Member__c> mapNewMember, Map<Id,Census_Member__c> mapOldMember)
    {
        CensusMemberHandler.onBeforeUpdateCensusMemberDateUpdate(mapNewMember, mapOldMember);
        CensusMemberHandler.onBeforeUpdateCensusPrimaryMemberUpdate(mapNewMember, mapOldMember);
    }
   


//Task  5 CensusUpdate
//INSERT PART
  public static void onAfterInsertCensusUpdate(List<Census_Member__c> lstCensusMem) 
    {
        List<Census__c> lstCen = new List<Census__c>();
        for(Census_Member__c objCen : lstCensusMem) 
        {
            if(objCen.vlocity_ins_IsPrimaryMember__c == True) 
            {
                Census__c objCensus = new Census__c();
                objCensus.Id = objCen.vlocity_ins_CensusId__c;
                objCensus.Primary_Member__c = objCen.Name;
                lstCen.add(objCensus);
            }
        }
        if(!lstCen.isEmpty())
        {
            update lstCen;
        }
    }
//Task  5 CensusUpdate
//UPDATE PART    
    public static void onAfterUpdateCensusUpdate(Map<Id,Census_Member__c> mapNewMember, Map<Id,Census_Member__c> mapOldMember) 
    {
        List<Census__c> lstCenUpdate = new List<Census__c>();
        for(Id CensId : mapNewMember.keySet())
        {
            Census_Member__c objCensusNew = mapNewMember.get(CensId);
            Census_Member__c objCensusOld = mapOldMember.get(CensId);
            if(objCensusNew.vlocity_ins_IsPrimaryMember__c == True) 
            {
                Census__c objCensu = new Census__c();
                objCensu.Id = objCensusNew.vlocity_ins_CensusId__c;
                objCensu.Primary_Member__c = objCensusNew.Name;
                lstCenUpdate.add(objCensu); 
            }
            if(objCensusNew.vlocity_ins_IsPrimaryMember__c == False) 
            {
                Census__c objCensu = new Census__c();
                objCensu.Id = objCensusNew.vlocity_ins_CensusId__c;
                objCensu.Primary_Member__c = NULL;
                lstCenUpdate.add(objCensu); 
            }
        }
            update lstCenUpdate;
    }
//END TASK 5


//Task  6 CensusMemberDateUpdate
//INSERT PART
  public static void onBeforeInsertCensusMemberDateUpdate(List<Census_Member__c> lstCensusMem) {
      Set<Id> setOfCensusIds = new Set<Id>();
      for(Census_Member__c objCensus : lstCensusMem) {
          setOfCensusIds.add(objCensus.vlocity_ins_CensusId__c);
      }
        Map<Id, Census__c> mapCensus = new Map<Id, Census__c>([SELECT vlocity_ins_EffectiveStartDate__c, vlocity_ins_EffectiveEndDate__c FROM Census__c WHERE Id IN : setOfCensusIds]);
        for( Census_Member__c objCensusMem : lstCensusMem)
        {
            if(objCensusMem.htcs_EffectiveDate__c <  mapCensus.get(objCensusMem.vlocity_ins_CensusId__c).vlocity_ins_EffectiveStartDate__c || objCensusMem.htcs_TermDate__c > mapCensus.get(objCensusMem.vlocity_ins_CensusId__c).vlocity_ins_EffectiveEndDate__c)
            {
                objCensusMem.addError('Invalid Dates,Plz enter correct dates');
            }
        }
        
    }
//Task  6 CensusMemberDateUpdate
//UPDATE PART    
    public static void onBeforeUpdateCensusMemberDateUpdate(Map<Id,Census_Member__c> mapNewMember, Map<Id,Census_Member__c> mapOldMember)
    {
        Map<Id, Census__c> mapCensus = new Map<Id, Census__c>([SELECT vlocity_ins_EffectiveStartDate__c, vlocity_ins_EffectiveEndDate__c FROM Census__c]);
        for(Id CensId : mapNewMember.keySet()) 
        {
            if(mapNewMember.get(CensId).htcs_EffectiveDate__c != NULL || mapNewMember.get(CensId).htcs_TermDate__c !=NULL)
            {
                if(mapNewMember.get(CensId).htcs_EffectiveDate__c <=  mapCensus.get(mapNewMember.get(CensId).vlocity_ins_CensusId__c).vlocity_ins_EffectiveStartDate__c || mapNewMember.get(CensId).htcs_TermDate__c >= mapCensus.get(mapNewMember.get(CensId).vlocity_ins_CensusId__c).vlocity_ins_EffectiveEndDate__c || mapNewMember.get(CensId).htcs_EffectiveDate__c == mapOldMember.get(CensId).htcs_EffectiveDate__c || mapNewMember.get(CensId).htcs_TermDate__c == mapOldMember.get(CensId).htcs_TermDate__c) 
                {
                    mapNewMember.get(CensId).addError('Check the dates,Enter it in correct way');
                }  
            }
            
        }
        
    }
//END TASK 6


//Task  7 CensusPrimaryMemberUpdate
//INSERT PART
  public  static void onBeforeInsertCensusPrimaryMemberUpdate(List<Census_Member__c> lstCensusMem) {
        set<Id> setPrimaryIds = new set<Id>();
        List<Census__c> lstCensus = [SELECT Id,Primary_Member__c FROM Census__c WHERE Primary_Member__c != NULL];
        for(Census__c objCensus : lstCensus) {
               setPrimaryIds.add(objCensus.Id);
        }
        for(Census_Member__c objCensusMem : lstCensusMem){
            if(setPrimaryIds.contains(objCensusMem.vlocity_ins_CensusId__c) && objCensusMem.vlocity_ins_IsPrimaryMember__c == TRUE){
                objCensusMem.addError('Primary memebr is present in the census');
            }
        }
    }
//Task  7 CensusPrimaryMemberUpdate
//UPDATE PART    
    public  static void onBeforeUpdateCensusPrimaryMemberUpdate(Map<Id,Census_Member__c> mapNewMember, Map<Id,Census_Member__c> mapOldMember) {
        set<Id> setPrimaryIds = new set<Id>();
        List<Census__c> lstCensus = [SELECT Id,Primary_Member__c FROM Census__c WHERE Primary_Member__c != NULL];
        for(Census__c objCensus : lstCensus) {
            setPrimaryIds.add(objCensus.Id);
        }
        for(Id censusId : mapNewMember.keyset()) {
            
            Census_Member__c objCensusNew = mapNewMember.get(censusId);
            Census_Member__c objCensusOld = mapOldMember.get(censusId);
            if(setPrimaryIds.contains(mapNewMember.get(censusId).vlocity_ins_CensusId__c) && objCensusNew.vlocity_ins_IsPrimaryMember__c != objCensusOld.vlocity_ins_IsPrimaryMember__c && objCensusNew.vlocity_ins_IsPrimaryMember__c == TRUE)
            {
                 mapNewMember.get(censusId).addError('Primary memebr is present in the census');
            }
        }
    }
//END TASK 7


//Task  7.2 CensusRollUpMember
//INSERT PART
      public static void onAfterInsertCensusRollUpMember(List<Census_Member__c> lstCensusMem) 
    {
        Set<Id> setOfCenIds = new Set<Id>();
        for(Census_Member__c objMemberCen : lstCensusMem) 
        {
            setOfCenIds.add(objMemberCen.vlocity_ins_CensusId__c);
        }
        Map<Id, Census__c> mapCensus = new Map<Id, Census__c>([SELECT Id,Name,Members__c FROM Census__c WHERE Id IN : setOfCenIds]);
        for( Census_Member__c objCenMember : lstCensusMem )
        {
            if(mapCensus.get(objCenMember.vlocity_ins_CensusId__c).Members__c != NULL)
            {
                mapCensus.put(objCenMember.vlocity_ins_CensusId__c, new Census__c(Id = objCenMember.vlocity_ins_CensusId__c , Members__c = mapCensus.get(objCenMember.vlocity_ins_CensusId__c).Members__c + ':' + objCenMember.Name ) );
            }
            else
            {
                mapCensus.put(objCenMember.vlocity_ins_CensusId__c, new Census__c(Id = objCenMember.vlocity_ins_CensusId__c , Members__c =  objCenMember.Name ));
            }
        }
        if(!mapCensus.isEmpty())
        {
            update mapCensus.values();
        }
      }
//Task  7.2 CensusRollUpMember
//UPDATE PART    
    public static void onAfterUpdateCensusRollUpMember(List<Census_Member__c> lstCensusMem, Map<Id,Census_Member__c> mapOldMember)
    {
        Set<Id> setOfIds = new Set<Id>();
        Map<Id, String> mapOfCensus = new Map<Id, String>();
        for(Census_Member__c objCenMem : lstCensusMem){
            setOfIds.add(objCenMem.vlocity_ins_CensusId__c);
        }
        for(Census_Member__c objCensusMember : [SELECT Id, Name, vlocity_ins_CensusId__c FROM Census_Member__c WHERE vlocity_ins_CensusId__c IN : setOfIds]){
            if(!mapOfCensus.containsKey(objCensusMember.vlocity_ins_CensusId__c)){
             mapOfCensus.put(objCensusMember.vlocity_ins_CensusId__c, objCensusMember.Name);
            }
            else{
             mapOfCensus.put(objCensusMember.vlocity_ins_CensusId__c, mapOfCensus.get(objCensusMember.vlocity_ins_CensusId__c) + ' : ' + objCensusMember.Name);
            }
        }
         List<Census__c> lstCensus = new List<Census__c>();
        for(Census_Member__c objCenIds : lstCensusMem){
            if(mapOldMember.get(objCenIds.Id).Name != objCenIds.Name){
            Census__c objCensus = new Census__c();
            objCensus.Id = objCenIds.vlocity_ins_CensusId__c;
            objCensus.Members__c = mapOfCensus.get(objCenIds.vlocity_ins_CensusId__c);
            lstCensus.add(objCensus);
            }
        }
          if(!lstCensus.isEmpty())
            update lstCensus;
    }
//END TASK 7.2
}